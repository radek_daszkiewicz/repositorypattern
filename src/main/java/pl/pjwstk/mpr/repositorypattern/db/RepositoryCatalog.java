/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.repositorypattern.db;

/**
 *
 * @author radek
 */
public interface RepositoryCatalog {
    
    public ClientRepository clients();
    
    public ItemRepository items();
    
    public OrderRepository orders();
    
}
