/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.repositorypattern.db;

import java.util.List;
import pl.pjwstk.mpr.repositorypattern.domain.Item;
import pl.pjwstk.mpr.repositorypattern.domain.OrderItems;

/**
 *
 * @author radek
 */
public interface OrderItemsRepository extends Repository<OrderItems> {

    public List<Item> getItemsByOrderId(Long id);
}
