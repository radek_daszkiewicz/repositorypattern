/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.repositorypattern.db.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pl.pjwstk.mpr.repositorypattern.db.OrderItemsRepository;
import pl.pjwstk.mpr.repositorypattern.domain.Item;
import pl.pjwstk.mpr.repositorypattern.domain.OrderItems;

/**
 *
 * @author radek
 */
public class HsqlOrderItemsRepository implements OrderItemsRepository {
    
    private Connection connection;
    
    private String selectByIdStmt = "SELECT id, order_id, item_id FROM order_items WHERE id=?";
    private String selectByOrderIdStmt = "SELECT id, order_id, item_id FROM order_items WHERE order_id=?";
    private String insertStmt = "INSERT INTO order_items(order_id, item_id) VALUES(?, ?)";
    private String updateStmt = "UPDATE order_items SET (order_id, item_id)=(?, ?) WHERE id=?";
    private String deleteStmt = "DELETE FROM order_items WHERE id=?";
    private String createOrderItemsTable = "CREATE TABLE order_items(id BIGINT, order_id BIGINT, item_id BIGINT)";
    
    private PreparedStatement selectById;
    private PreparedStatement selectByOrderId;
    private PreparedStatement insert;
    private PreparedStatement update;
    private PreparedStatement delete;
    
    public HsqlOrderItemsRepository(Connection connection) {
        this.connection = connection;
        
        try {
         
            selectById = connection.prepareStatement(selectByIdStmt);
            selectByOrderId = connection.prepareStatement(selectByOrderIdStmt);
            insert = connection.prepareStatement(insertStmt);
            update = connection.prepareStatement(updateStmt);
            delete = connection.prepareStatement(deleteStmt);
            
            ResultSet rs = connection.getMetaData().getTables(null, null, null, null) ;
            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("order_items")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createOrderItemsTable);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Item> getItemsByOrderId(Long id) {
        List<Item> items = new ArrayList<Item>();
        
        try {
            selectByOrderId.setLong(1, id);
            ResultSet rs = selectByOrderId.executeQuery();
            while(rs.next()) {
                
                HsqlItemRepository ir = new HsqlItemRepository(connection);
                Item i = ir.withId(rs.getInt("item_id"));
                
                items.add(i);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    @Override
    public OrderItems withId(int id) {
        
        OrderItems oi = new OrderItems();
        try {
            
            selectById.setLong(1, id);
            ResultSet rs = selectById.executeQuery();
            while (rs.next()) {
                oi.setId(rs.getLong("id"));
                oi.setOrder_id(rs.getLong("order_id"));
                oi.setItem_id(rs.getLong("item_id"));
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oi;
    }

    @Override
    public void add(OrderItems entity) {
        
        try {
            
            insert.setLong(1, entity.getOrder_id());
            insert.setLong(2, entity.getItem_id());
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void modify(OrderItems entity) {
        try {
            
            update.setLong(1, entity.getOrder_id());
            update.setLong(2, entity.getItem_id());
            update.setLong(3, entity.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(OrderItems entity) {
        try {
            delete.setLong(1, entity.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
