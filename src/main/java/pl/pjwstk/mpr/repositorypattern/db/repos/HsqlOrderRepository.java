/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.repositorypattern.db.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pl.pjwstk.mpr.repositorypattern.db.OrderRepository;
import pl.pjwstk.mpr.repositorypattern.domain.Address;
import pl.pjwstk.mpr.repositorypattern.domain.Client;
import pl.pjwstk.mpr.repositorypattern.domain.Item;
import pl.pjwstk.mpr.repositorypattern.domain.Order;

/**
 *
 * @author radek
 */
public class HsqlOrderRepository implements OrderRepository {
    private Connection connection;
    
    private String selectByIdStmt = "SELECT id, client_id, address_id, order_items_id FROM order WHERE id=?";
    private String selectByClientIdStmt = "SELECT id, client_id, address_id, order_items_id FROM order WHERE client_id=?";
    private String selectByAddressIdStmt = "SELECT id, client_id, address_id, order_items_id FROM order WHERE address_id=?";
    private String insertStmt = "INSERT INTO order(client_id, address_id, order_items_id) VALUES (?, ?, ?)";
    private String updateStmt = "UPDATE order SET (client_id, addess_id, order_items_id)=(?, ?, ?)";
    private String deleteStmt = "DELETE order WHERE id=?";
    private String getOrderItemsIdStmt = "SELECT id, order_id, item_id FROM order_items WHERE order_id=?";
    private String createOrderTable = "CREATE TABLE order(id BIGINT, client_id BIGINT, address_id BIGINT, order_items_id BIGINT)";
    
    private PreparedStatement selectById;
    private PreparedStatement selectByClientId;
    private PreparedStatement selectByAddressId;
    private PreparedStatement insert;
    private PreparedStatement update;
    private PreparedStatement delete;
    private PreparedStatement getOrderItemsId;
    
    public HsqlOrderRepository (Connection connection) {
        this.connection = connection;
        
        try {
            selectById = connection.prepareStatement(selectByIdStmt);
            selectByClientId = connection.prepareStatement(selectByClientIdStmt);
            selectByAddressId = connection.prepareStatement(selectByAddressIdStmt);
            insert = connection.prepareStatement(insertStmt);
            update = connection.prepareStatement(updateStmt);
            delete = connection.prepareStatement(deleteStmt);
            getOrderItemsId = connection.prepareStatement(getOrderItemsIdStmt);
            
            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("order")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createOrderTable);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Order> withClient(Client c) {
        List<Order> orders = new ArrayList<Order>();
        try {
            selectByClientId.setLong(1, c.getId());
            ResultSet rs = selectByClientId.executeQuery();
            while(rs.next()) {
                Order o = new Order();
                o.setId(rs.getLong("id"));
                o.setClient(c);
                
                HsqlAddressRepository ar = new HsqlAddressRepository(connection);
                Address a = ar.withId(rs.getInt("address_id"));
                o.setDeliveryAddress(a);
                
                HsqlOrderItemsRepository oir = new HsqlOrderItemsRepository(connection);
                List<Item> items = oir.getItemsByOrderId(o.getId());
                o.setItems(items);
                
                orders.add(o);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public List<Order> withAddress(Address a) {
        
        List<Order> orders = new ArrayList<Order>();
        try {
            selectByAddressId.setLong(1, a.getId());
            ResultSet rs = selectByAddressId.executeQuery();
            while(rs.next()) {
                
                Order o = new Order();
                o.setId(rs.getLong("id"));
                
                HsqlClientRepository cr = new HsqlClientRepository(connection);
                Client c = cr.withId(rs.getInt("client_id"));
                o.setClient(c);
                
                o.setDeliveryAddress(a);
                
                HsqlOrderItemsRepository oir = new HsqlOrderItemsRepository(connection);
                List<Item> items = oir.getItemsByOrderId(o.getId());
                o.setItems(items);
                
                orders.add(o);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Order withId(int id) {
        
        Order o = new Order();
        try {
            selectById.setLong(1, id);
            ResultSet rs = selectById.executeQuery();
            while(rs.next()) {
                o.setId(rs.getLong("id"));
                
                HsqlClientRepository cr = new HsqlClientRepository(connection);
                Client c = cr.withId(rs.getInt("client_id"));
                o.setClient(c);
                
                HsqlAddressRepository ar = new HsqlAddressRepository(connection);
                Address a = ar.withId(rs.getInt("address_id"));
                o.setDeliveryAddress(a);
                
                HsqlOrderItemsRepository oir = new HsqlOrderItemsRepository(connection);
                List<Item> items = oir.getItemsByOrderId(rs.getLong("order_id"));
                o.setItems(items);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return o;
    }

    @Override
    public void add(Order entity) {
        try {
            insert.setLong(1, entity.getId());
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void modify(Order entity) {
        try {
            update.setLong(1, entity.getClient().getId());
            update.setLong(2, entity.getDeliveryAddress().getId());
            
            getOrderItemsId.setLong(1, entity.getId());
            ResultSet rs = getOrderItemsId.executeQuery();
            update.setLong(3, rs.getLong("id"));
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Order entity) {
        try {
            delete.setLong(1, entity.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
