/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.repositorypattern.db.catalogs;

import java.sql.Connection;
import pl.pjwstk.mpr.repositorypattern.db.ClientRepository;
import pl.pjwstk.mpr.repositorypattern.db.ItemRepository;
import pl.pjwstk.mpr.repositorypattern.db.OrderRepository;
import pl.pjwstk.mpr.repositorypattern.db.repos.HsqlClientRepository;
import pl.pjwstk.mpr.repositorypattern.db.RepositoryCatalog;
import pl.pjwstk.mpr.repositorypattern.db.repos.HsqlItemRepository;
import pl.pjwstk.mpr.repositorypattern.db.repos.HsqlOrderRepository;

/**
 *
 * @author radek
 */
public class HsqlRepositoryCatalog implements RepositoryCatalog {

    Connection connection;

    public HsqlRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }
    
    @Override
    public ClientRepository clients() {
        return new HsqlClientRepository(connection);
    }

    @Override
    public ItemRepository items() {
        return new HsqlItemRepository(connection);
    }

    @Override
    public OrderRepository orders() {
        return new HsqlOrderRepository(connection);
    }
    
}
